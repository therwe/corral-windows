(* 
 This script collects windows from all screens onto
- the screen with the mouse,
- the screen with the current front application, or
- the primary screen.
and arranges them there, either
- maximized, 
- stacked, and as large as possible, or
- stacked in the center, sized to 3/4 of usable screen width and height.
Alternatively, it can
- only move screens that are (partially or fully) outside the target screen into it, or
- scale the layout of all windows on all screens onto the target screen (while shrinking window widths and heights to scale).


KNOWN BUG: If several windows have the same name, only 1 of them will be processed!
Example: Of the open windows "Untitled", "Untitled", "Untitled", "Mozilla Firefox", "Mozilla Firefox", "Mozilla Firefox", 
only one "Untitled" and one "Mozilla Firefox" will be moved and/or resized.


Other Notes
PRIMARY SCREEN: 			the one with the menu bar in System Preferences
MAIN SCREEN: 				the one with the active window
SCREEN WITH CURSOR:		where the mouse is currently
DESKTOP:						all screens together, arranged as shown in System Preferences
MOUSE POSITION: 			coordinates within desktop
VISIBLE FRAME:				frame screen space not used by menu bar or Dock
 
Variable 'CA':				to make shorter lines
Variables '…_LLO':		reminder that this is Lower Left Offset (relevant for screens Y)
Finder Windows have to be treated via "tell app Finder…" because TotalFinder reports them twice or more to System Events
Finder Windows adjacent to menu bar always report position 1 px lower -??
*)


use AppleScript version "2.4" -- Yosemite (10.10) or later
use scripting additions
use framework "AppKit" -- for NSScreen

global primaryScreenFrame
global targetFrame
global winsToProcess


(*
	-- DEBUG
	set windowResultList to ¬
		{"X_w" & tab & tab & "Y_w" & tab & tab & ¬
			"W_w" & tab & tab & "H_w" & return & ¬
			"X_r" & tab & tab & "Y_r" & tab & tab & ¬
			"W_r" & tab & tab & "H_r" & tab & tab & "name" & return}
*)


-- SELECT WHAT YOU WANT
-- the action (how to position/resize the windows)
set resizeTypeList to ¬
	{"ALLmaximized", "ALLstackedMax", "ALLstackedSmall", "ALLstackedCenter", "OFFBOUNDSMovedResized", "ALLScaledLayout"}
set resizeType to (choose from list resizeTypeList with title ¬
	"Action Selection" with prompt "How shall the windows be arranged:" default items ¬
	"ALLstackedMax") as text -- 																											dialog to chose action
-- the screen to move windows onto

set screenList to ¬
	{"Screen with cursor (where the mouse is currently)", ¬
		"Main screen (with the active window)", ¬
		"Primary screen (with menu bar in system preferences)"}
set targetScreen to (choose from list screenList with title ¬
	"Screen Selection" with prompt "Where to gather all windows:" default items ¬
	"Screen with cursor (where the mouse is currently)") as text -- 													dialog to chose target screen

-- the stacking order
set stackByProcess to false
display dialog "How shall the windows be stacked?" buttons {"Stack As Is", "By Process"} default button "Stack As Is"
if the return = "By Process" then set stackByProcess to true -- 														dialog to chose stacking order


-- GET WINDOWS LIST and SCREEN COORDINATES
my getWindowList() --																															returns windows to process 

set theFrames to my getScreenFrames(targetScreen) --																			returns visible frame of target screen


-- SET VARIABLES for ALL WINDOWS/ACTIONS
set winCount to (count winsToProcess)

set {{{X_target, Y_target_LLO}, {W_target, H_target}}, ¬
	{{X_primary, Y_primary_LLO}, {W_primary, H_primary}}} to theFrames --											put into individual variables

set Y_target to (H_primary - Y_target_LLO - H_target) --																	convert Y from frame => desktop

if resizeType = "ALLstackedMax" then
	set {W_windowNew, H_windowNew} ¬
		to {(W_target - ((winCount - 1) * 22)), (H_target - ((winCount - 1) * 22))} --						max size within stacking; 22 = stacking offset
	
else if (resizeType = "ALLstackedSmall") then
	set {W_windowNew, H_windowNew} ¬
		to {W_target * 0.75, H_target * 0.75} --																					size NNN * screen width, height
	
else if resizeType = "ALLstackedCenter" then
	set {X_center, Y_center} ¬
		to {(X_target + (W_target / 2)), (Y_target + (H_target / 2))} --												center of visible frame
	set {W_windowNew, H_windowNew} ¬
		to {(W_target * 0.75), (H_target * 0.75)} --																				size NNN * screen width, height
	
else if resizeType = "OFFBOUNDSMovedResized" then --																			calculate W,H of target
	set R_target to X_target + W_target
	set B_target to H_primary - Y_target_LLO
	
else if (resizeType = "ALLScaledLayout") then --																				scale layout to fit on target screen (shrinks window bounds,even if all are already on target screen)
	tell application "Finder" to ¬
		set {X_desktop, Y_desktop, W_desktop, H_desktop} to desktop's window's bounds --					DESKTOP bounds (X, Y not needed)
	set {X_scaleFactor, Y_scaleFactor} to {(W_target / W_desktop), (H_target / H_desktop)} --			W and H ratios
	
end if


(*
	-- DEBUG
	-- !! this only works if the 'if…'/'else if…'/'end if…' lines above are commented out, but then some values will not be as intended !!
	display dialog "" buttons {"OK"} default button "OK" default answer ¬
		"X_target" & tab & X_target & return & ¬
		"Y_target_LLO" & tab & Y_target_LLO & return & ¬
		"W_target" & tab & W_target & return & ¬
		"H_target" & tab & H_target & return & ¬
		return & ¬
		"X_primary" & tab & X_primary & return & ¬
		"Y_primary_LLO" & tab & Y_primary_LLO & return & ¬
		"W_primary" & tab & W_primary & return & ¬
		"H_primary" & tab & H_primary & return & ¬
		return & ¬
		"Y_target" & tab & Y_target & return & ¬
		"X_center" & tab & X_center & return & ¬
		"Y_center" & tab & Y_center & return & ¬
		"R_target" & tab & R_target & return & ¬
		"B_target" & tab & B_target & return & ¬
		return & ¬
		"X_desktop" & tab & X_desktop & return & ¬
		"Y_desktop" & tab & Y_desktop & return & ¬
		"W_desktop" & tab & W_desktop & return & ¬
		"H_desktop" & tab & H_desktop & return & ¬
		return & ¬
		"W_windowNew" & tab & W_windowNew & return & ¬
		"H_windowNew" & tab & H_windowNew & return & ¬
		return & ¬
		"X_scaleFactor" & tab & X_scaleFactor & return & ¬
		"Y_scaleFactor" & tab & Y_scaleFactor as text --																		you may laugh - but that helped a lot!
*)


-- INDIVIDUAL WINDOWS
repeat with i from 1 to winCount --																									loop through windows
	set thisWin to (winsToProcess's item i)
	
	-- 
	--  PROBLEM: only one window is processed of several (of the same app?) with non-unique names 
	-- "You'd need to arrange your script so that it counted how many windows there were with a certain 
	-- name and use 'whose' references in a repeat loop, eg. 'window n whose name is thisName'. 
	-- Of course you'd have to make sure you didn't do anything to a window to muck up the numbering 
	-- of others with the same name. And some windows don't have names, so you also have to be careful 
	-- how you ask!"   -- Nigel Garvey  https://macscripter.net/viewtopic.php?pid=194770#p194770
	-- 
	
	
	-- GET CURRENT WINDOW'S FRAME
	try --																																				works on all windows but Finder's
		tell application "System Events" to ¬
			set {{X_windowOld, Y_windowOld}, {W_windowOld, H_windowOld}} ¬
				to {(thisWin's position), (thisWin's size)}
	on error --																																		probably a Finder window
		tell application "Finder" to ¬
			set {X_windowOld, Y_windowOld, R_windowOld, B_windowOld} to thisWin's bounds
		set {(W_windowOld), (H_windowOld)} ¬
			to {R_windowOld - X_windowOld, B_windowOld - Y_windowOld} -- calculate frame from bounds
	end try
	--> X_windowOld, Y_windowOld, W_windowOld, H_windowOld
	--> for Finder windows also R_windowOld, B_windowOld
	
	-- CALCULATE NEW POSITION, SIZE
	-- calculate only frame for nonFinder windows, will be converted later for Finder windows
	if resizeType = "ALLmaximized" then --																							ALL windows: MAXIMIZED
		set {{X_windowNew, Y_windowNew}, {W_windowNew, H_windowNew}} ¬
			to {{X_target, Y_target}, {W_target, H_target}}
		
	else if (resizeType = "ALLstackedMax") or (resizeType = "ALLstackedSmall") then --						ALL windows: STACKED (max or small)
		-- size was set outside loop: as large as possible with stacking, or NNN * screen width and height
		set X_windowNew to X_target + (22 * (i - 1))
		set Y_windowNew to Y_target + (22 * (i - 1))
		
	else if resizeType = "ALLstackedCenter" then --																				ALL windows: STACK from CENTER
		set stackOffset to (22 * ((winCount - 1) / 2)) -- 																	11 = (stacking offset / 2)
		set myShift to (22 * (i - 1)) --																									22 = stacking offset
		set X_windowNew to X_center - (W_windowNew / 2) - stackOffset + (22 * (i - 1))
		set Y_windowNew to Y_center - (H_windowNew / 2) - stackOffset + (22 * (i - 1))
		
	else if resizeType = "OFFBOUNDSMovedResized" then --																		windows that are out of target bounds: MOVE
		-- copy current values to new
		set {X_windowNew, Y_windowNew, W_windowNew, H_windowNew} ¬
			to {X_windowOld, Y_windowOld, W_windowOld, H_windowOld} --													now we only change …windowNEW for X, Y, W, H, R, B
		-- if too large for target frame (with menubar), resize first
		if W_windowNew > W_target then set W_windowNew to W_target --													too wide: shrink to available width
		if H_windowNew > H_target then set H_windowNew to H_target --													too high: shrink to available height
		
		-- if (partially) out of target frame (with menubar), move window fully in	  							window border out of target border to …
		if (X_windowNew < X_target) then set X_windowNew to X_target --												… left: move right
		if ((X_windowNew + W_windowNew) > R_target) then ¬
			set X_windowNew to R_target - W_windowNew --																			… right: move left			
		if (Y_windowNew < Y_target) then set Y_windowNew to Y_target --												… top: move down
		if ((Y_windowNew + H_windowNew) > Y_target + H_target) then ¬
			set Y_windowNew to B_target - H_windowNew --																			… bottom: move up
		
	else if (resizeType = "ALLScaledLayout") then
		set {X_windowNew, Y_windowNew, W_windowNew, H_windowNew} to ¬
			{X_windowOld * X_scaleFactor, Y_windowOld * Y_scaleFactor, W_windowOld * X_scaleFactor, H_windowOld * Y_scaleFactor}
		
	end if
	
	
	-- SET POSITION & SIZE or BOUNDS          set only changed values? but would need multiple 'if's, so possibly slower 
	if {{X_windowOld, Y_windowOld}, {W_windowOld, H_windowOld}} ¬
		≠ {{X_windowNew, Y_windowNew}, {W_windowNew, H_windowNew}} then --											only set if changed
		try --																																			works on all windows but Finder's
			-- System Events position autoresizes
			tell application "System Events" to ¬
				set {(thisWin's size), (thisWin's position)} ¬
					to {{W_windowNew, H_windowNew}, {X_windowNew, Y_windowNew}} -- 									old size larger => resize first
			tell application "System Events" to ¬
				set {(thisWin's position), (thisWin's size)} ¬
					to {{X_windowNew, Y_windowNew}, {W_windowNew, H_windowNew}} -- 									new size larger => position first
		on error --																																	probably a Finder window
			try
				tell application "Finder" to set thisWin's bounds ¬
					to {X_windowNew, (Y_windowNew + 20), (X_windowNew + W_windowNew), (Y_windowNew + H_windowNew)} -- frame => Finder bounds
			on error
				-- could catch 'real' errors here
			end try
		end try
	end if
	
	
	-- BRING TO FRONT (stacks by application)
	if stackByProcess = true then
		tell application "System Events"
			try --																																		works on all windows but Finder's
				perform action "AXRaise" of thisWin
			on error --																																probably a Finder window
				try
					tell application process "Finder"
						set index of thisWin to 1
						perform action "AXRaise" of window 1
					end tell
				on error
					-- could catch 'real' errors here
				end try
			end try
		end tell
	end if
	
	
	(*
		-- DEBUG
		tell application "System Events"
			set {X_result, Y_result} to  thisWin's position
			try
				set {W_result, H_result} to thisWin's size
			on error
				set {W_result, H_result} to {(X_windowNew + W_windowNew), (Y_windowNew + H_windowNew)}
			end try
			set name_result to  thisWin's name
			set end of windowResultList to ¬
				(round X_windowNew) & tab & tab & (round Y_windowNew) & tab & tab & ¬
				(round W_windowNew) & tab & tab & (round H_windowNew) & return & ¬
				(round X_result) & tab & tab & (round Y_result) & tab & tab & ¬
				(round W_result) & tab & tab & (round H_result) & tab & tab & ¬
				name_result & return
		end tell
	*)
	
end repeat -- windows loop

-- DEBUG
(*
	display dialog "" default answer windowResultList as text ¬
		buttons ("----------------------------- OK -----------------------------") ¬
		default button "----------------------------- OK -----------------------------"
*)



on getScreenFrames(targetScreen)
	-- get frames necessary to move/resize
	-- based on a tip from StefanK  https://macscripter.net/viewtopic.php?pid=194723#p194723
	-- modification for screen IDs: https://macscripter.net/viewtopic.php?id=46336
	set CA to current application
	
	set primaryScreenFrame to ¬
		(CA's NSScreen's screens()'s firstObject()'s frame()) --															PRIMARY SCREEN frame
	
	if targetScreen = "Screen with cursor (where the mouse is currently)" then
		set mouseCoordinates to CA's NSEvent's mouseLocation() --															MOUSE POSITION
		set allScreens to CA's NSScreen's screens() --																			array of screens
		repeat with aScreen in allScreens --																							loop through array
			if CA's NSPointInRect(mouseCoordinates, aScreen's frame()) then --										if cursor is here
				set mouseScreenFrame to (aScreen's visibleFrame()) --														SCREEN WITH CURSOR visible frame
				exit repeat
			end if
		end repeat
		set targetFrame to mouseScreenFrame
		
	else if targetScreen = "Main screen (with the active window)" then
		set targetFrame to (CA's NSScreen's mainScreen()'s visibleFrame()) --										MAIN SCREEN visible frame
		
	else if targetScreen = "Primary screen (with menu bar in system preferences)" then
		set targetFrame to primaryScreenFrame
		
	end if
	
	return {targetFrame, primaryScreenFrame}
end getScreenFrames



on getWindowList()
	-- GET LIST OF OPEN WINDOWS
	
	tell application "System Events"
		set visibProcs to a reference to (processes whose visible = true and name ≠ "Finder") --		visible apps excluding Finder
		set nonMiniNonFinderWins to (windows of visibProcs whose value of attribute "AXMinimized" is false) --	their non-minimized windows
	end tell
	
	set winsToProcess to {}
	repeat with i from 1 to count nonMiniNonFinderWins --																	flatten list
		repeat with j from 1 to count (nonMiniNonFinderWins's item i)
			set end of winsToProcess to ((nonMiniNonFinderWins's item i)'s item j)
		end repeat
	end repeat
	
	tell application "Finder"
		set FinderWins to windows --																										Finder windows
	end tell
	
	repeat with k from 1 to (count FinderWins)
		set end of winsToProcess to (FinderWins's item k) --																	add Finder windows to list
	end repeat
	
	return winsToProcess
end getWindowList
