# Corral windows
An AppleScript to corral all windows onto one screen, with various options to move, resize, and stack them.

Useful after changing multi-monitor setups.

This script collects windows from all screens onto
* the screen with the mouse, or
* the screen with the current front application, or
* the primary screen

and arranges them there, either
* maximized, or
* stacked, and as large as possible, or
* stacked in the center, sized to 3/4 of usable screen width and height.

Alternatively, it can

* only move screens that are (partially or fully) outside the target screen into it, or
* scale the layout of all windows on all screens onto the target screen (while shrinking window widths 
and heights to scale).

**KNOWN BUG: If several windows have the same name, only 1 of them will be processed**! Example: Of the open windows "Untitled", "Untitled", "Untitled", "Mozilla Firefox", "Mozilla Firefox", "Mozilla Firefox", only one "Untitled" and one "Mozilla Firefox" will be moved and/or resized.

# Usage:
You can run the script and set the options in three dialogs.

Or you can hardcode the options and use the script in anything that runs Applescripts. It runs, for example
* as a droplet
* in an Automator action
* as a Service (in an Automator workflow)
* in Keyboard Maestro
* a Butler menu item.

And of course, you can improve it (let me know!).

*Whatever runs the script will need the necessary permissions* (System Preferences > Security & Privacy > Privacy > Accessibility).
